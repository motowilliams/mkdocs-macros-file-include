"""
To installed the code
Install on the system with `pip install -e mkdocs_test_macro.py`
"""

from setuptools import setup

setup(
    name='mkdocs-macros-file-include',
    version='0.0.1',
    description="Pluglet to load files from any location on the filesystem",
    packages=['mkdocs-macros-file-include'],
    license='MIT',
    author='Eric Williams'
)
